import * as React from 'react';
import * as ReactDOM from 'react-dom';
import '../node_modules/bootstrap/dist/css/bootstrap.css';
import './scss/main.scss';

import Layout from './components/layout';

ReactDOM.render(
    <Layout />,
    document.getElementById('root')
);