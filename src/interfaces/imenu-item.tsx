interface IMenuItem{
    name: string,
    link: string
}
export default IMenuItem;