import IMedia from './imedia';
interface IMall {
    _id: string;
    name: string;
    image: string;
    distance: number;
    address: string;
    subname: string;
    events: string[];
    banners: IMedia[];
    coordinates: number[];
    logo: string;
    topNavImage: string;
    topNavLogo: string;
}
export default IMall;