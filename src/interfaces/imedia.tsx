interface IMedia{
    imageUrl: string;
    type: number;
    videoType: string;
    videoThumbnail: string;
    videoId: string;
}
export default IMedia;