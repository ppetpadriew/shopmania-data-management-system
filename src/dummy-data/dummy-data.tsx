import IMall from '../interfaces/IMall';
export default class DummyData{
    getMalls(): IMall[]{
        return [
        {
            _id: '1',
            name: 'Siam Paragon',
            image: 'http://test-app.shopmania.in.th:8080/images/malls/siam-paragon-retina.jpg',
            distance: 3.0,
            address: '999/9 Rama I Rd, Phathumwan, Phathumwan, Bangkok 10330, Thailand',
            subname: 'Siam Paragon subname',
            events: [],
            banners: [],
            coordinates: [],
            logo: 'http://test-app.shopmania.in.th:8080/images/malls/central-world-logo-retina.jpg',
            topNavImage: '',
            topNavLogo: ''
        },
        {
            _id: '2',
            name: 'Central World',
            image: 'http://test-app.shopmania.in.th:8080/images/malls/central-world-retina.jpg',
            distance: 5.3,
            address: '999/9 Rama I Rd, Phathumwan, Phathumwan, Bangkok 10330, Thailand',
            subname: 'Central World subname',
            events: [],
            banners: [],
            coordinates: [],
            logo: '',
            topNavImage: '',
            topNavLogo: ''
        },
        {
            _id: '3',
            name: 'Central Embassy',
            image: 'http://test-app.shopmania.in.th:8080/images/malls/central-embassy-retina.jpg',
            distance: 5.1,
            address: '999/9 Rama I Rd, Phathumwan, Phathumwan, Bangkok 10330, Thailand',
            subname: 'Central Embassy subname',
            events: [],
            banners: [],
            coordinates: [],
            logo: '',
            topNavImage: '',
            topNavLogo: ''
        },
        {
            _id: '4',
            name: 'Central Chidlom',
            image: 'http://test-app.shopmania.in.th:8080/images/malls/central-chidlom-retina.jpg',
            distance: 8.2,
            address: '999/9 Rama I Rd, Phathumwan, Phathumwan, Bangkok 10330, Thailand',
            subname: 'Central Chidlom subname',
            events: [],
            banners: [],
            coordinates: [],
            logo: '',
            topNavImage: '',
            topNavLogo: ''
        }
    ];
    }
}