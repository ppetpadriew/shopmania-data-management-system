import * as React from 'react';
interface BodyProps{

}
interface BodyStates{

}
export default class Body extends React.Component<BodyProps, BodyStates>{
    render(): JSX.Element{
        return(
            <div className="container">
                {this.props.children}
            </div>
        );
    }
}
