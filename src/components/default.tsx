import * as React from 'react';
interface DefaultProps{

}
interface DefaultStates{

}
export default class Default extends React.Component<DefaultProps, DefaultStates>{
    render(): JSX.Element{
        return(
            <h1>Default Content</h1>
        );
    }
}
