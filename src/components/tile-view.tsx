import * as React from 'react';
import { Link } from 'react-router';
interface TileViewProps{
    dataSet: any[]
}
interface TileViewStates{

}
export default class TileView extends React.Component<TileViewProps, TileViewStates>{
    getTiles(): JSX.Element[]{
        let tiles: JSX.Element[];
        tiles = this.props.dataSet.map(function (data) {
            return(
                <div className="tile col-sm-6 col-md-4" key={data._id}>
                    <Link to={`/malls/${data._id}`}>
                        <div className="thumbnail">
                            <img src={data.image} alt=""/>
                            <div className="caption">
                                <h3>{data.name}</h3>
                                <span className="glyphicon glyphicon-map-marker"></span>
                                <span className="distance">{data.distance} KM</span>
                            </div>
                        </div>
                    </Link>
                </div>
            );
        });
        return tiles;
    }
    render(): JSX.Element{
        return(
            <div>
                <div className="tile-view-header"></div>
                <div className="tile-view-body row">
                    {this.getTiles()}
                </div>
            </div>
        );
    }
}