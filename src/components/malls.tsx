import * as React from 'react';
import TileView from './tile-view';
import { Breadcrumb } from 'react-bootstrap';
import IMall from '../interfaces/imall';
import DummyData from '../dummy-data/dummy-data';
interface MallsProps{

}
interface MallsStates{

}

export default class Malls extends React.Component<MallsProps, MallsStates>{
    malls: IMall[];
    dataSet: any[];
    constructor(props: MallsProps, context: any){
        super(props, context);
        let dummyData = new DummyData();
        this.malls = dummyData.getMalls();
        this.dataSet = this.malls;
    }
    render(): JSX.Element{
        return(
            <div>
                <Breadcrumb>
                    <Breadcrumb.Item active>Malls</Breadcrumb.Item>
                </Breadcrumb>
                <TileView dataSet={this.dataSet} />
            </div>
        );
    }
}
