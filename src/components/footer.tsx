import * as React from 'react';
import { Navbar, Nav, NavItem, NavDropdown, MenuItem } from 'react-bootstrap';
interface FooterProps{

}
interface FooterStates{

}
export default class Footer extends React.Component<FooterProps, FooterStates>{
    render(): JSX.Element{
        return(
            <footer className="footer">
                <Navbar inverse >
                    <Navbar.Text>
                        @Copyright 2017 Peeratchai Petpadriew
                    </Navbar.Text>
                </Navbar>
            </footer>
        );
    }
}