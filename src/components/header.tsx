import * as React from 'react';
import { Navbar, Nav, NavItem, NavDropdown, MenuItem } from 'react-bootstrap';
import IMenuItem from '../interfaces/imenu-item';
interface HeaderProps{

}
interface HeaderStates{

}
export default class Header extends React.Component<HeaderProps, HeaderStates>{
    masterDataMenuItems: IMenuItem[];
    constructor(props: HeaderProps, context: any) {
        super(props, context);
        this.masterDataMenuItems = [
            {
                name: 'Malls',
                link: '#/malls'
            },
            {
                name: 'Shops',
                link: '#/shops'
            },
            {
                name: 'Products',
                link: '#/products'
            },
            {
                name: 'Coupons',
                link: '#/coupons'
            },
            {
                name: 'Advertisements',
                link: '#/advertisements'
            },
            {
                name: 'Advertisement Histories',
                link: '#/advertisement-histories'
            }
        ];
    }
    getMasterDataMenuItems(): JSX.Element[]{
        let items: JSX.Element[];
        let eventKey: number;
        items = this.masterDataMenuItems.map((menuItem: IMenuItem, i: number) => {
            eventKey = 1.00 + (i/100);
            return <MenuItem eventKey={eventKey} key={menuItem.name} href={menuItem.link}>{menuItem.name}</MenuItem>
        });
        return items;
    }
    render(): JSX.Element{
        return(
            <Navbar inverse collapseOnSelect>
                <Navbar.Header>
                    <Navbar.Brand>
                        <a href="#">ShopMania Data Management System</a>
                    </Navbar.Brand>
                    <Navbar.Toggle />
                </Navbar.Header>
                <Navbar.Collapse>
                    <Nav>
                        <NavDropdown eventKey={1} title="Master Data" id="basic-nav-dropdown">
                            {this.getMasterDataMenuItems()}
                        </NavDropdown>
                        <NavItem eventKey={2} href="#">Link</NavItem>
                        <NavItem eventKey={3} href="#">Link</NavItem>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        );
    }
}