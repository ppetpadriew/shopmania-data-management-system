import * as React from 'react';
import Header from './header';
import Footer from './footer';
import { Router, Route, IndexRoute, hashHistory } from 'react-router';
import Body from './body';
import Default from './default';
import Malls from './malls';
import Mall from './mall';
interface LayoutProps {
}
interface LayoutStates {
    tasks: string[]
}
export default class Layout extends  React.Component<LayoutProps, LayoutStates>{
    render(): JSX.Element{
        return (
            <div>
                <Header />
                <Router history={hashHistory}>
                    <Route path="/" component={Body}>
                        <IndexRoute component={Default}></IndexRoute>
                        <Route path="malls" component={Malls}></Route>
                        <Route path="malls/:_id" component={Mall}></Route>
                    </Route>
                </Router>
                <Footer />
            </div>
        );
    }
}