import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { Breadcrumb, Form, FormGroup, FormControl, ControlLabel, Image, Grid, Row, Col, Button } from 'react-bootstrap';
import IMall from '../interfaces/imall';
import ChangeEvent = React.ChangeEvent;
import DummyData from '../dummy-data/dummy-data';
import ObjectManager from '../shopmania-lib/object-manager';
interface MallRouteProps{
    _id: string
}
interface MallRouteParamsProps{

}
interface MallStates extends IMall{

}
export default class Mall extends React.Component<RouteComponentProps<MallRouteProps,MallRouteParamsProps>, MallStates>{
    malls: IMall[];
    objectManager: ObjectManager;
    constructor(props: RouteComponentProps<MallRouteProps,MallRouteParamsProps>, context: any){
        super(props, context);
        this.objectManager = new ObjectManager();
        let dummyData = new DummyData();
        this.malls = dummyData.getMalls();
        let mall: IMall
        mall = this.malls.filter(function (mall) {
            for(let prop in mall){
                if(prop === '_id' && mall[prop] === props.params._id){
                    return true;
                }
            }
            return false;
        })[0];
        this.state = mall;

    }
    handleChange(e: ChangeEvent<any>): void{
        let value: string = '';
        if(this.objectManager.hasProperty(e.target, 'value')){
            value = e.target.value;
        }
        this.setState({
            [e.target.name]: e.target.value
        });
    }
    previewImage(e: ChangeEvent<any>): void{
        let fieldName = e.target.name;
        let files: FileList = e.target.files;
        let fileReader: FileReader = new FileReader();
        fileReader.readAsDataURL(files[0]);
        fileReader.onloadend = (e: ProgressEvent) => {
            this.setState({
                [fieldName]: fileReader.result
            });
        };

    }
    render(): JSX.Element{
        let logo:string = '/images/img-placeholder.png';
        let topNavImage:string = '/images/img-placeholder.png';
        let topNavLogo:string = '/images/img-placeholder.png';
        if(this.state.logo){
            logo = this.state.logo;
        }

        if(this.state.topNavImage){
            topNavImage = this.state.topNavImage;
        }

        if(this.state.topNavLogo){
            topNavLogo = this.state.topNavLogo;
        }
        return(
          <div>
              <Breadcrumb>
                  <Breadcrumb.Item href="#">Malls</Breadcrumb.Item>
                  <Breadcrumb.Item active>{this.props.params._id}</Breadcrumb.Item>
              </Breadcrumb>
              <Form horizontal>
                  <Grid>
                      <Row>
                          <Col xs={12} md={6}><Image src={this.state.image} thumbnail /></Col>
                          <Col xs={12} md={6}>
                              <FormGroup controlId="nameForm">
                                  <Col componentClass={ControlLabel} sm={2}>
                                      Name
                                  </Col>
                                  <Col sm={10}>
                                      <FormControl type="text" name="name" value={this.state.name} onChange={this.handleChange.bind(this)} />
                                  </Col>
                              </FormGroup>
                              <FormGroup controlId="addressForm">
                                  <Col componentClass={ControlLabel} sm={2}>
                                      Address
                                  </Col>
                                  <Col sm={10}>
                                      <FormControl type="text" name="address" value={this.state.address} onChange={this.handleChange.bind(this)} />
                                  </Col>
                              </FormGroup>

                              <FormGroup controlId="subnameForm">
                                  <Col componentClass={ControlLabel} sm={2}>
                                      Subname
                                  </Col>
                                  <Col sm={10}>
                                      <FormControl type="text" name="subname" value={this.state.subname} onChange={this.handleChange.bind(this)} />
                                  </Col>
                              </FormGroup>

                              <FormGroup controlId="logoForm">
                                  <Col componentClass={ControlLabel} sm={2}>
                                      Logo
                                  </Col>
                                  <Col sm={4}>
                                      <Image src={logo} thumbnail />
                                  </Col>
                                  <Col sm={6}>
                                      <FormControl type="file" name="logo" onChange={this.previewImage.bind(this)} />
                                  </Col>
                              </FormGroup>

                              <FormGroup controlId="topNavImageForm">
                                  <Col componentClass={ControlLabel} sm={2}>
                                      Top Nav. Image
                                  </Col>
                                  <Col sm={4}>
                                      <Image src={topNavImage} thumbnail />
                                  </Col>
                                  <Col sm={6}>
                                      <FormControl type="file" name="topNavImage" onChange={this.previewImage.bind(this)} />
                                  </Col>
                              </FormGroup>

                              <FormGroup controlId="topNavLogoForm">
                                  <Col componentClass={ControlLabel} sm={2}>
                                      Top Nav. Logo
                                  </Col>
                                  <Col sm={4}>
                                      <Image src={topNavLogo} thumbnail />
                                  </Col>
                                  <Col sm={6}>
                                      <FormControl type="file" name="topNavLogo" onChange={this.previewImage.bind(this)} />
                                  </Col>
                              </FormGroup>
                          </Col>
                      </Row>
                      <Row>
                          <Col sm={12}>
                              <Button bsStyle="primary" bsSize="large" block>Save</Button>
                          </Col>
                      </Row>
                  </Grid>
              </Form>
          </div>
        );
    }
}