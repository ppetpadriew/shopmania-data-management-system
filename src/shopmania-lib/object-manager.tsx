export default class ObjectManager{
    hasProperty(obj: Object, search: string): boolean {
        for(let prop in obj){
            if(prop === search){
                return true;
            }
        }
        return false;
    }
}